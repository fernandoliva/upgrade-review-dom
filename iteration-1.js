//1.1
// const countries = ['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela'];
// const $$ul = document.createElement('ul');

// for (key of countries){
//     let $$li = document.createElement('li');
//     $$li.innerText = key;
//     $$ul.appendChild($$li);
// }

// document.body.appendChild($$ul);

//1.2
// const $$pRemove = document.querySelector('.fn-remove-me');
// $$pRemove.remove();

//1.3
// const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];
// const $$ul = document.createElement('ul');
// const $$div = document.querySelector('[data-function = "printHere"]');

// for (key of cars){
//     let $$li = document.createElement('li');
//     $$li.innerText = key;
//     $$ul.appendChild($$li);
// }

// $$div.appendChild($$ul);

//1.4
// const countries = [{
//         title: 'Random title',
//         imgUrl: 'https://picsum.photos/300/200?random=1'
//     },
//     {
//         title: 'Random title',
//         imgUrl: 'https://picsum.photos/300/200?random=2'
//     },
//     {
//         title: 'Random title',
//         imgUrl: 'https://picsum.photos/300/200?random=3'
//     },
//     {
//         title: 'Random title',
//         imgUrl: 'https://picsum.photos/300/200?random=4'
//     },
//     {
//         title: 'Random title',
//         imgUrl: 'https://picsum.photos/300/200?random=5'
//     }
// ];

// const $$box = document.querySelector('.box');

// for (key of countries) {
//     let $$div = document.createElement('div');
//     let $$title = document.createElement('h4');
//     let $$image = document.createElement('img');
//     $$title.innerText = key.title;
//     $$image.src = key.imgUrl;
//     $$div.className = 'image';
//     $$div.appendChild($$title) && $$div.appendChild($$image);
//     $$box.appendChild($$div);
// }

// //1.5
// const $$lastButton = document.querySelector('.eraseLastButton');
// $$lastButton.addEventListener('click', eraseLastElement);

// function eraseLastElement() {
//     let $$lastDiv = document.querySelector('div.image:last-child');
//     $$lastDiv.parentElement.removeChild($$lastDiv);
// }

// //1.6

// const $$allDiv = document.querySelectorAll('.image');
// let counter = 1;

// for (let key of $$allDiv) {
//     let $$button = document.createElement('button');
//     $$button.textContent = 'Elemento ' + counter;
//     $$button.className = 'deleteButton';
//     document.body.appendChild($$button);

//     $$button.addEventListener('click', eraseElement);
//     function eraseElement() {
//         key.remove(key);
//         $$button.remove();
//     }
//     counter++;
// }